package ru.smlab;

import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

import static org.testng.AssertJUnit.assertEquals;

public class Test {

    @org.testng.annotations.Test
    public void haveStudents() throws IOException {

        Path path = Paths.get("C:/projects/homeWorks/Question/src/main/resources/student.txt");

        String read = Files.readAllLines(path).get(0);
        Assert.assertNotNull(read, "Список студентов пустой");
    }

    @org.testng.annotations.Test
    public void haveQuestion() throws IOException {

        Path path = Paths.get("C:/projects/homeWorks/Question/src/main/resources/question.txt");

        String read = Files.readAllLines(path).get(0);
        Assert.assertNotNull(read, "Список с вопросами  пустой");
    }

    @org.testng.annotations.Test
    public void haveTest() throws IOException {

        Path path = Paths.get("C:/projects/homeWorks/Question/src/main/resources/" + LocalDate.now() + ".txt");

        String read = Files.readAllLines(path).get(0);
        Assert.assertNotNull(read, "Файл с тестом пустой");
    }


}

