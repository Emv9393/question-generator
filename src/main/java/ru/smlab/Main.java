package ru.smlab;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) throws IOException {
        String  studentsFilePath = "C:/projects/homeWorks/Question/src/main/resources/student.txt",
                questionsFilePath = "C:/projects/homeWorks/Question/src/main/resources/question.txt",
                resultFilePath = "C:/projects/homeWorks/Question/src/main/resources/" + LocalDate.now() + ".txt";

        File resultFile = new File(resultFilePath);
        resultFile.createNewFile();

        List<String> students = Files.readAllLines(Paths.get(studentsFilePath));
        List<String> questions = Files.readAllLines(Paths.get(questionsFilePath));
        int qSize = questions.size()-1;
        List<String> result = new ArrayList<>();
        for (String student: students) {
            result.add(student + "- " + questions.get(ThreadLocalRandom.current().nextInt(0, qSize)) + '\n');
        }
        try (BufferedWriter fileWriter = new BufferedWriter(new FileWriter(resultFilePath))) {
            for (String str : result) {
                fileWriter.write(str);
            }
        }
        System.out.println("Файл сформирован");
    }

}